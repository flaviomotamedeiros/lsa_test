#include <stdio.h>
#include <stdlib.h>

int main()
{

#ifdef A
   char *str;

   /* Initial memory allocation */
   str = (char *) malloc(15);
   strcpy(str, "tutorialspoint");
   printf("String = %s,  Address = %u\n", str, str);
#endif

#ifdef B
   free(str);
#endif
   
#ifdef C
    // Testing...
#endif
    
   return(0);
}